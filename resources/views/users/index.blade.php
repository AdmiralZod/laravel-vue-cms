@extends('layouts.dashboard')

@section('content')
     <!-- Page Heading -->
     <h1 class="h3 mb-2 text-gray-800">Users</h1>
                  

     <!-- DataTales Example -->
     <div class="card shadow mb-4">
         <div class="card-header py-3">
             <h6 class="m-0 font-weight-bold text-primary">This is the users</h6>
         </div>
     </div>
     <div id="app">
        <users-component></users-component>
     </div>
    
@endsection
