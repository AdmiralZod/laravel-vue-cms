<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function index()
    {

        return response()->json(['data' => User::orderByDesc('created_at')->get()]);
    }

    public function store()
    {
        $validator = Validator::make(request()->all(),[
            'email' => 'required|email|unique:users',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required',
            'postal_code' => 'required|numeric',
            'contact_number' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required|min:8|confirmed'
        ]);

        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $request = request()->all();
        $request['password'] = bcrypt(request('password'));
        $user = User::create($request);

        return response()->json(['data' => $user]);
    }

    public function show(User $user)
    {

        return response()->json($user);
    }

    public function update(User $user)
    {
        $validator = Validator::make(request()->all(),[
            'email' => 'required|email|unique:users,email,'.$user->id,
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'address' => 'required',
            'postal_code' => 'required|numeric',
            'contact_number' => 'required',
            'username' => 'required|unique:users,username,'.$user->id,
            'password' => 'nullable|min:8|confirmed'
        ]);

        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $request = request()->all();
        if($password = request('password')){
            $request['password'] = bcrypt($password);
        }

        $user->update($request);

        return response()->json($user);
    }

    public function destroy(User $user){
        $user->delete();

        return response()->json('Success', 200);
    }

    public function deleteMultiple(){
        
        if($userIds = request('user_ids')){
            foreach($userIds as $userId){
                $user = User::find($userId);
                if($user){
                    $user->delete();
                }
            }
            return response()->json("Success delete");
        }

        return response()->json("No user to delete");

    }

}
