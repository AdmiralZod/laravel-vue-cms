## About Laravel-vue-cms

This application is laravel backend with vue js component only for users cms.
Your choice to setup on your local development server(xampp or homestead). If you are using xampp you need to download composer.


Run this commands on terminal on project directory

1. composer install
2.  php -r "file_exists('.env') || copy('.env.example', '.env');"
3. Update your .env keys according to your local server db (DB_DATABASE, DB_USERNAME, DB_PASSWORD).
4. php artisan key:generate
5. php artisan migrate
6. php artisan db:seed
7. php artisan passport:install


default user:
email : admin@example.com
username : admin
password: 12345678